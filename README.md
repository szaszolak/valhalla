# Valhalla

## Description
It is a sample social media like application. Registered users can enlist themselves as two main professions
- warriors
- farmers

Every user can browse each list of each profession. 
An additional module is created to serve weather forecast for each day.

In the [merge](https://gitlab.com/szaszolak/valhalla/merge_requests/2) request, one can observe changes that were required to extract weather prophecies module into Rail's engine.

## Quick start 

### Docker setup
Easiest way to spin up app, is to use docker & docker-compose (versions 3 or higher)

1. `docker-compose build`
2. `docker-compose run web scripts/wait-for-it.sh db:5432 --  "rake db:create db:migrate db:seed"`
3. `docker-compose up`