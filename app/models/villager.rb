# frozen_string_literal: true

# == Schema Information
#
# Table name: villagers
#
#  id                     :bigint(8)        not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  villagerable_type      :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  villagerable_id        :integer
#
# Indexes
#
#  index_villagers_on_email                                  (email) UNIQUE
#  index_villagers_on_reset_password_token                   (reset_password_token) UNIQUE
#  index_villagers_on_villagerable_type_and_villagerable_id  (villagerable_type,villagerable_id)
#

class Villager < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  belongs_to :villagerable, polymorphic: true, optional: true, validate: true
end
