# frozen_string_literal: true

# == Schema Information
#
# Table name: farmers
#
#  id           :bigint(8)        not null, primary key
#  crop         :enum             not null
#  harvest_time :enum             not null
#

class Farmer < ApplicationRecord
  enum crop: {
    grain: 'grain',
    cheese: 'cheese',
    meat: 'meat',
    wool: 'wool'
  }

  enum harvest_time: {
    january: 'january',
    february: 'february',
    march: 'march',
    april: 'april',
    may: 'may',
    june: 'june',
    july: 'july',
    august: 'august',
    september: 'september',
    october: 'october',
    november: 'november',
    december: 'december'
  }

  has_one :villager, as: :villagerable

  validates :crop, :harvest_time, presence: true
end
