# frozen_string_literal: true

# == Schema Information
#
# Table name: warriors
#
#  id               :bigint(8)        not null, primary key
#  raids            :integer          default(0)
#  role             :enum             default("shieldman")
#  weapon_of_choice :enum             default("axe")
#

class Warrior < ApplicationRecord
  enum weapon_of_choice: {
    sword: 'sword',
    axe:   'axe',
    spear: 'spear'
  }

  enum role: {
    shieldman: 'shieldman',
    spearman:  'spearman',
    berserker: 'berserker'
  }

  has_one :villager, as: :villagerable

  validates :raids, numericality: { greater_than_or_equal_to: 0 }
  validates :weapon_of_choice, :role, presence: true
end
