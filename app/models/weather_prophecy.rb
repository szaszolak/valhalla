# frozen_string_literal: true

# == Schema Information
#
# Table name: weather_prophecies
#
#  id          :bigint(8)        not null, primary key
#  rain        :enum             default("moderate")
#  temperature :enum             default("moderate")
#  wind        :enum             default("moderate")
#  created_at  :date
#

class WeatherProphecy < ApplicationRecord
  enum rain: {
    heavy: 'heavy',
    moderate: 'moderate',
    none: 'none'
  }, _suffix: true

  enum temperature: {
    cold: 'cold',
    moderate: 'moderate',
    hot: 'hot'
  }, _suffix: true

  enum wind: {
    strong: 'strong',
    moderate: 'moderate',
    light: 'light'
  }, _suffix: true

  validates :rain, :temperature, :wind, :created_at, presence: true
end

