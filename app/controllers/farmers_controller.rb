# frozen_string_literal: true

class FarmersController < ApplicationController
  def index
    render locals: { farmers: Farmer.all }
  end

  def new
    render locals: { farmer: Farmer.new }
  end

  def create
    RegisterProfession.call(profession_class: Farmer, params: farmer_params, villager: current_villager)
    redirect_to farmers_path, notice: "#{current_villager.email} was successfully registerd as farmer."
  end

  private

  def farmer_params
    params.require(:farmer).permit(:crop, :harvest_time)
  end
end
