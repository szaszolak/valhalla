# frozen_string_literal: true

# == Schema Information
#
# Table name: weather_prophecies
#
#  id          :bigint(8)        not null, primary key
#  rain        :enum             default("moderate")
#  temperature :enum             default("moderate")
#  wind        :enum             default("moderate")
#  created_at  :date
#


require 'rails_helper'

RSpec.describe WeatherProphecy, type: :model do
  describe 'validations' do
    describe 'rain' do
      context 'is present' do
        it 'is valid' do
          expect(build(:weather_prophecy, rain: 'heavy')).to be_valid
        end
      end

      context 'is absent' do
        it 'is invalid' do
          expect(build(:weather_prophecy, rain: nil)).to_not be_valid
        end
      end

      context 'has unknown value' do
        it 'raises error' do
          expect do
            build(:weather_prophecy, rain: 'unknown')
          end.to raise_error(ArgumentError, "'unknown' is not a valid rain")
        end
      end
    end
  end
end
