# create 3 static users
berserker = Warrior
              .create!(
                role: Warrior.roles.fetch(:berserker),
                weapon_of_choice: Warrior.weapon_of_choices.fetch(:axe),
                raids: rand(5)
              )

shieldman = Warrior
              .create!(
                role: Warrior.roles.fetch(:shieldman),
                weapon_of_choice: Warrior.weapon_of_choices.fetch(:sword),
                raids: rand(5)
              )

farmer = Farmer
              .create!(
                crop: Farmer.crops.fetch(:wool), 
                harvest_time: Farmer.harvest_times.fetch(:may)
              )

olaf = Villager.create(email: 'olaf@hugin.com', password: 'secret', villagerable: shieldman)
thorun = Villager.create(email: 'thorun@hugin.com', password: 'secret', villagerable: berserker)
sven = Villager.create(email: 'sven@hugin.com', password: 'secret', villagerable: farmer)

#generate 3 more rand warriors
warrior_roles = Warrior.roles.values
weapons = Warrior.weapon_of_choices.values

3.times do
  proffesion = Warrior.create!(role: warrior_roles.sample, weapon_of_choice: weapons.sample, raids: rand(5))
  Villager.create!(email: Faker::Internet.unique.email, password: Faker::Crypto.md5, villagerable: proffesion)
end

#generate 3 more rand farmers
crops = Farmer.crops.values
harvest_times = Farmer.harvest_times.values

3.times do
  proffesion = Farmer.create!(crop: crops.sample, harvest_time: harvest_times.sample)
  Villager.create!(email: Faker::Internet.unique.email, password: Faker::Crypto.md5, villagerable: proffesion)
end
